﻿using Microsoft.AspNetCore.Mvc;
using ProductApp.MVVCore.Exceptions;
using ProductApp.MVVCore.Models;
using ProductApp.MVVCore.Services;

namespace ProductApp.MVVCore.Controllers
{
    public class ProductController : Controller
    {
        // List<Product> productDetails;
        //readonly IProductRepsitory _productRepsitory;
        /* public ProductController(IProductRepsitory productRepsitory)
         {

             _productRepsitory = productRepsitory;
         }*/
        readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            List<Product> productsList = _productService.GetProducts();

            return View(productsList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            try
            {
                int productAddStatus = _productService.AddProduct(product);
                if (productAddStatus == 1)
                {

                    return RedirectToAction("Index");
                }
                else
                {
                    return Redirect("Create");
                }
            }
            catch(ProductExistException pex)
            {
                return StatusCode(409, pex.Message);
            }
           

        }

        [HttpGet]

        public ActionResult Delete(int id)
        {
            Product? productDetailsToDelete = _productService.DeleteProductById(id);
            return View(productDetailsToDelete);
        }
        [HttpPost]

        public ActionResult Delete(Product product)
        {
            int productDeletedStatus = _productService.DeleteProduct(product.Id);
            if (productDeletedStatus == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Product? productDetails = _productService.getProductById(id);
            if (productDetails != null)
            {
                return View(productDetails);
            }
            else
            {
                return View();
            }
        }
    }
}
