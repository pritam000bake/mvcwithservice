﻿using ProductApp.MVVCore.Exceptions;
using ProductApp.MVVCore.Models;
using ProductApp.MVVCore.Repository;
namespace ProductApp.MVVCore.Services

{
    public class ProductService : IProductService
    {
        readonly IProductRepsitory _productRepsitory;
        public ProductService(IProductRepsitory productRepsitory)
        {
            _productRepsitory = productRepsitory;
        }
        public int AddProduct(Product product)
        {
            Product? productDetailsExist = _productRepsitory.GetProductByName(product.Name);
            if (productDetailsExist == null)
            {
                return _productRepsitory.AddProduct(product);
            }
            else
            {
                throw new ProductExistException($"{product.Name}AlreadyExist!!");
            }
        }

        public int DeleteProduct(int id)
        {
            return _productRepsitory.DeleteProduct(id);
        }

        public Product? DeleteProductById(int id)
        {
            return _productRepsitory.DeleteProductById(id);
        }

        public Product? getProductById(int id)
        {
            return _productRepsitory.getProductById(id);
        }

        public List<Product> GetProducts()
        {
            return _productRepsitory.GetProducts();
        }
    }
}
