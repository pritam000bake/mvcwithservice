﻿using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Services
{
    public interface IProductService
    {
        List<Product> GetProducts();
        int AddProduct(Product product);
        Product? DeleteProductById(int id);
        int DeleteProduct(int id);
        Product? getProductById(int id);
    }
}
