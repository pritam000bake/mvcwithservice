﻿using ProductApp.MVVCore.Context;
using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Repository
{
    public class ProductRepsitory : IProductRepsitory
    {
        ProductDbContext _productDbContext;
        /*public ProductRepsitory()
         {
          //   _products = new List<Product>();
             _productDbContext = new ProductDbContext();

         }*/
        public ProductRepsitory(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }


        #region AddProduct in List
        /*public bool AddProduct(Product product)
        {
            if (_products.Count > 0)
            {
                int maxId = _products.Max(p => p.Id);
                product.Id = maxId + 1;
            }
            else
            {
                product.Id = 1;
            }
          
            _products.Add(product);
            return true;

            
        }*/
        #endregion

        #region AddProduct in Database
        public int AddProduct(Product product)
        {
            //if (_productDbContext.Products.Count() > 0)
            //{
            //    int maxId = _productDbContext.Products.Max(p => p.Id);
            //    product.Id = maxId + 1;
            //}
            //else
            //{
            //    product.Id = 1;
            //}
            _productDbContext.Products.Add(product);
            return _productDbContext.SaveChanges();



        }
        #endregion

        #region Delete Product in List
        /* public bool DeleteProduct(int id)
         {
            Product? deleteProductDetails =  getProductById(id);
             if (deleteProductDetails != null)
             {
                 return _products.Remove(deleteProductDetails);
             }
             else
             {
                 return false;
             }

         }*/
        #endregion

        #region Delete Product in Database
        public int DeleteProduct(int id)
        {
            Product? deleteProductDetails = getProductById(id);
            if (deleteProductDetails != null)
            {
                _productDbContext.Products.Remove(deleteProductDetails);
                return _productDbContext.SaveChanges();
            }
            else
            {
                return 0;
            }

        }
        #endregion

        #region Delete Product By Id from List
        /* public Product? DeleteProductById(int id)
         {
             return _products.Find(p => p.Id == id);
         }*/
        #endregion

        #region Delete Product By Id from Database
        public Product? DeleteProductById(int id)
        {
            return _productDbContext.Products.Where(p => p.Id == id).FirstOrDefault();
        }
        #endregion

        #region Get product by id from list
        /*public Product? getProductById(int id)
        {
            return _products.Find(p => p.Id == id);
        }*/
        #endregion
        #region Get Product by Id from Database
        public Product? getProductById(int id)
        {
            return _productDbContext.Products.Where(p => p.Id == id).FirstOrDefault();
        }

        public Product? GetProductByName(string? name)
        {
            return _productDbContext.Products.Where(u => u.Name==name).FirstOrDefault();
        }
        #endregion

        #region Get Products from List
        /*
         * List<Product> IProductRepsitory.GetProducts()
        {
            return _products;
        }*/
        #endregion

        #region get Product from Db
        public List<Product> GetProducts()
        {
            return _productDbContext.Products.ToList();
        }
        #endregion
    }
}
